package com.epsi.integrationContinueBackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VoteListeRepository extends JpaRepository<VoteListe, Long> {

}
