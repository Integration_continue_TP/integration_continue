package com.epsi.integrationContinueBackend.repository;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class VoteKey implements Serializable {

    Long userId;

    Long voteListId;

    public VoteKey() {
    }

    public VoteKey(Long userId, Long voteListId) {
        this.userId = userId;
        this.voteListId = voteListId;

    }

    public Long getUserId() {
        return userId;
    }

    public Long getVoteListId() {
        return voteListId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setVoteListId(Long voteListId) {
        this.voteListId = voteListId;
    }
}
