package com.epsi.integrationContinueBackend.repository;

import javax.persistence.*;


@Entity
@Table(name = "VOTE")
public class Vote {

    @EmbeddedId
    private VoteKey id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne
    @MapsId("voteListId")
    @JoinColumn(name = "VOTE_LISTE_ID")
    private VoteListe voteListe;

    public Vote() {
    }

    public VoteKey getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public VoteListe getVoteListe() {
        return voteListe;
    }

    public void setId(VoteKey id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setVoteListe(VoteListe voteListe) {
        this.voteListe = voteListe;
    }
}
