package com.epsi.integrationContinueBackend.repository;

import javax.persistence.*;

@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USERNAME")
    private String userNname;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "TYPE")
    private boolean type;

    public User() { }

    public User(Long id, String userNname, String password, boolean type) {
        this.id = id;
        this.userNname = userNname;
        this.password = password;
        this.type = type;
    }

    public Long getId() { return id; }

    public String getUserNname() { return userNname; }

    public String getPassword() { return password; }

    public boolean isType() { return type; }

    public void setId(Long id) { this.id = id; }

    public void setUserNname(String userNname) { this.userNname = userNname; }

    public void setPassword(String password) { this.password = password; }

    public void setType(boolean type) { this.type = type; }

}


