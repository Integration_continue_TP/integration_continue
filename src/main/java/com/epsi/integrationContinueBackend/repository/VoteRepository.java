package com.epsi.integrationContinueBackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, Long> {
    @Query(
            value = "SELECT count (v.user.id) as votes, v.voteListe.name as choix " +
                    "FROM Vote v GROUP BY choix"
    )
    List<ListeVote> findAllResults();

    interface ListeVote{
        int getVotes();
        String getChoix();
    }

}
