package com.epsi.integrationContinueBackend.repository;

import javax.persistence.*;

@Entity
@Table(name = "VOTE_LISTE")
public class VoteListe {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;


    @Column(name = "NAME", unique = true)
    private String name;

    public VoteListe() {
    }

    public VoteListe(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() { return id; }

    public String getName() { return name; }

    public void setId(Long id) { this.id = id; }

    public void setName(String name) { this.name = name; }
}
