package com.epsi.integrationContinueBackend.controller;

import com.epsi.integrationContinueBackend.repository.User;
import com.epsi.integrationContinueBackend.repository.VoteRepository;
import com.epsi.integrationContinueBackend.services.VoteServices;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class VoteController {

    private final VoteServices voteServices;

    public VoteController( final VoteServices voteServices) {
        this.voteServices = voteServices;
    }

    @GetMapping(value = "/voteResult")
    public List<VoteRepository.ListeVote> getAllResults()
    {
        return voteServices.getVoteResults();
    }

    @GetMapping(value = "/users")
    public List<User> getAllUsers(){
        return voteServices.getAllUser();
    }

}
