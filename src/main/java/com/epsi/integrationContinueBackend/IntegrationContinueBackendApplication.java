package com.epsi.integrationContinueBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class IntegrationContinueBackendApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(IntegrationContinueBackendApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(IntegrationContinueBackendApplication.class);
    }

}
