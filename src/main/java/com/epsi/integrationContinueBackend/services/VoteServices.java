package com.epsi.integrationContinueBackend.services;

import com.epsi.integrationContinueBackend.repository.User;
import com.epsi.integrationContinueBackend.repository.UserRepository;
import com.epsi.integrationContinueBackend.repository.VoteListeRepository;
import com.epsi.integrationContinueBackend.repository.VoteRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VoteServices {
    private final UserRepository userRepository;
    private final VoteListeRepository voteListeRepository;
    private final VoteRepository voteRepository;

    public VoteServices(UserRepository userRepository, VoteListeRepository voteListeRepository, VoteRepository voteRepository) {
        this.userRepository = userRepository;
        this.voteListeRepository = voteListeRepository;
        this.voteRepository = voteRepository;
    }

    public List<VoteRepository.ListeVote> getVoteResults(){
        return voteRepository.findAllResults();
    }

    public List<User> getAllUser(){
        return userRepository.findAll();
    }


}
